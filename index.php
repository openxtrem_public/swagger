<?php
$path_to_project = "./vendor/openxtrem/swagger";
$path_to_openapi = "./includes/documentation.yml";
$title           = "APIs documentation";
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?= $title ?></title>
  <link rel="stylesheet" type="text/css" href="<?= $path_to_project ?>/swagger-ui.css">
  <link rel="icon" type="image/x-icon" href="<?= $path_to_project ?>/favicon.ico" sizes="16x16" />
  <link rel="icon" type="image/x-icon" href="<?= $path_to_project ?>/favicon.ico" sizes="32x32" />
  <style>
    html {
      box-sizing: border-box;
      overflow: -moz-scrollbars-vertical;
      overflow-y: scroll;
    }

    *,
    *:before,
    *:after {
      box-sizing: inherit;
    }

    body {
      margin: 0;
      background: #fafafa;
    }
	
	#swagger-ui > div > div:nth-child(2) > div.information-container.wrapper {
      background-image: url('<?= $path_to_project ?>/logo_mb.svg');
      background-repeat: no-repeat;
      background-position: top;
      background-position: right;
      background-size: contain;
    }
  </style>
</head>

<body>
<div id="swagger-ui"></div>
<script src="<?= $path_to_project ?>/swagger-ui-bundle.js"></script>
<script src="<?= $path_to_project ?>/swagger-ui-standalone-preset.js"></script>
<script>
  window.onload = function () {
    const ui = SwaggerUIBundle({
      url:         "<?= $path_to_openapi ?>",
      dom_id:      '#swagger-ui',
      deepLinking: true,
      presets:     [
        SwaggerUIBundle.presets.apis
      ],
      filter:      true
    })
    window.ui = ui
  }
</script>
</body>
</html>
